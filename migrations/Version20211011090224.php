<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211011090224 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chambre_froide DROP FOREIGN KEY FK_54D1B648DB53ACE1');
        $this->addSql('DROP INDEX idx_54d1b648db53ace1 ON chambre_froide');
        $this->addSql('CREATE INDEX IDX_5E245AA7DB53ACE1 ON chambre_froide (officine_id_id)');
        $this->addSql('ALTER TABLE chambre_froide ADD CONSTRAINT FK_54D1B648DB53ACE1 FOREIGN KEY (officine_id_id) REFERENCES officine (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chambre_froide DROP FOREIGN KEY FK_5E245AA7DB53ACE1');
        $this->addSql('DROP INDEX idx_5e245aa7db53ace1 ON chambre_froide');
        $this->addSql('CREATE INDEX IDX_54D1B648DB53ACE1 ON chambre_froide (officine_id_id)');
        $this->addSql('ALTER TABLE chambre_froide ADD CONSTRAINT FK_5E245AA7DB53ACE1 FOREIGN KEY (officine_id_id) REFERENCES officine (id)');
    }
}
