<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211011091543 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Création des Entités DataTemp et DataHygro';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE data_hygro (id INT AUTO_INCREMENT NOT NULL, chambre_froide_id_id INT NOT NULL, date_heure DATETIME NOT NULL, valeur NUMERIC(5, 2) NOT NULL, validation VARBINARY(255) NOT NULL, INDEX IDX_BA745200CAB4BF24 (chambre_froide_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_temp (id INT AUTO_INCREMENT NOT NULL, chambre_froide_id_id INT NOT NULL, date_heure DATETIME NOT NULL, valeur NUMERIC(5, 2) NOT NULL, validation VARBINARY(255) NOT NULL, INDEX IDX_B01C4E28CAB4BF24 (chambre_froide_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data_hygro ADD CONSTRAINT FK_BA745200CAB4BF24 FOREIGN KEY (chambre_froide_id_id) REFERENCES chambre_froide (id)');
        $this->addSql('ALTER TABLE data_temp ADD CONSTRAINT FK_B01C4E28CAB4BF24 FOREIGN KEY (chambre_froide_id_id) REFERENCES chambre_froide (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE data_hygro');
        $this->addSql('DROP TABLE data_temp');
    }
}
