<?php

namespace App\Tests;

use App\Entity\DataTemp;
use PHPUnit\Framework\TestCase;

class DataTempUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $dataTemp = new dataTemp();
        $date1 = new \DateTime('2021-10-08 15:30:00');

        $dataTemp  ->setDateHeure($date1)
            ->setValeur('5.00')
            ->setValidation(true);

        $this->assertTrue($dataTemp->getDateHeure() === $date1);
        $this->assertTrue($dataTemp->getValeur() === '5.00');
        $this->assertTrue($dataTemp->getValidation() === true);
    }

    public function testIsFalse()
    {
        $dataTemp = new dataTemp();
        $date1 = new \DateTime('2021-10-08 15:30:00');
        $date2 = new \DateTime('2019-10-08 17:30:00');

        $dataTemp  ->setDateHeure($date1)
            ->setValeur('5.00')
            ->setValidation(true);

        $this->assertFalse($dataTemp->getDateHeure() === $date2);
        $this->assertFalse($dataTemp->getValeur() === '1.00');
        $this->assertFalse($dataTemp->getValidation() === false);
    }

    public function testIsEmpty()
    {
        $dataTemp = new dataTemp();

        $this->assertEmpty($dataTemp->getDateHeure());
        $this->assertEmpty($dataTemp->getValeur());
        $this->assertEmpty($dataTemp->getValidation());
    }

}
