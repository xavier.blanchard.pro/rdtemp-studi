<?php

namespace App\Tests;

use App\Entity\DataHygro;
use PHPUnit\Framework\TestCase;

class DataHygroUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $dataHygro = new dataHygro();
        $date1 = new \DateTime('2021-10-08 15:30:00');

        $dataHygro  ->setDateHeure($date1)
            ->setValeur('5.00')
            ->setValidation(true);

        $this->assertTrue($dataHygro->getDateHeure() === $date1);
        $this->assertTrue($dataHygro->getValeur() === '5.00');
        $this->assertTrue($dataHygro->getValidation() === true);
    }

    public function testIsFalse()
    {
        $dataHygro = new dataHygro();
        $date1 = new \DateTime('2021-10-08 15:30:00');
        $date2 = new \DateTime('2019-10-08 17:30:00');

        $dataHygro  ->setDateHeure($date1)
            ->setValeur('5.00')
            ->setValidation(true);

        $this->assertFalse($dataHygro->getDateHeure() === $date2);
        $this->assertFalse($dataHygro->getValeur() === '1.00');
        $this->assertFalse($dataHygro->getValidation() === false);
    }

    public function testIsEmpty()
    {
        $dataHygro = new dataHygro();

        $this->assertEmpty($dataHygro->getDateHeure());
        $this->assertEmpty($dataHygro->getValeur());
        $this->assertEmpty($dataHygro->getValidation());
    }

}
