<?php

namespace App\Tests;

use App\Entity\Officine;
use PHPUnit\Framework\TestCase;

class OfficineTest extends TestCase
{
    public function testIsTrue()
    {
        $officine = new officine();

        $officine  ->setRaisonSociale('Officine-XXX-001')
            ->setAdresse('1 rue des tests')
            ->setCodePostal('75000')
            ->setVille('Paris')
            ->setTelephone('0102030405');

        $this->assertTrue($officine->getRaisonSociale() ==='Officine-XXX-001');
        $this->assertTrue($officine->getAdresse() === '1 rue des tests');
        $this->assertTrue($officine->getCodePostal() === '75000');
        $this->assertTrue($officine->getVille() === 'Paris');
        $this->assertTrue($officine->getTelephone() === '0102030405');
    }

    public function testIsFalse()
    {
        $officine = new officine();

        $officine  ->setRaisonSociale('Officine-XXX-001')
            ->setAdresse('1 rue des tests')
            ->setCodePostal('75000')
            ->setVille('Paris')
            ->setTelephone('0102030405');

        $this->assertFalse($officine->getRaisonSociale() ==='Officine-XXX-002');
        $this->assertFalse($officine->getAdresse() === '1 rue des tests faux');
        $this->assertFalse($officine->getCodePostal() === '85000');
        $this->assertFalse($officine->getVille() === 'Toulouse');
        $this->assertFalse($officine->getTelephone() === '0504030201');
    }

    public function testIsEmpty()
    {
        $officine = new officine();

        $this->assertEmpty($officine->getRaisonSociale());
        $this->assertEmpty($officine->getAdresse());
        $this->assertEmpty($officine->getCodePostal());
        $this->assertEmpty($officine->getVille());
        $this->assertEmpty($officine->getTelephone());
    }

}
