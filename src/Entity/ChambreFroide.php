<?php

namespace App\Entity;

use App\Repository\ChambreFroideRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChambreFroideRepository::class)
 */
class ChambreFroide
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $TempMin;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $TempMax;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $HygroMin;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $HygroMax;

    /**
     * @ORM\ManyToOne(targetEntity=Officine::class, inversedBy="chambreFroide")
     * @ORM\JoinColumn(nullable=false)
     */
    private $OfficineID;

    /**
     * @ORM\OneToMany(targetEntity=DataTemp::class, mappedBy="ChambreFroideID")
     */
    private $dataTemps;

    /**
     * @ORM\OneToMany(targetEntity=DataHygro::class, mappedBy="ChambreFroideID")
     */
    private $dataHygros;

    public function __construct()
    {
        $this->dataTemps = new ArrayCollection();
        $this->dataHygros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getTempMin(): ?string
    {
        return $this->TempMin;
    }

    public function setTempMin(string $TempMin): self
    {
        $this->TempMin = $TempMin;

        return $this;
    }

    public function getTempMax(): ?string
    {
        return $this->TempMax;
    }

    public function setTempMax(string $TempMax): self
    {
        $this->TempMax = $TempMax;

        return $this;
    }

    public function getHygroMin(): ?string
    {
        return $this->HygroMin;
    }

    public function setHygroMin(string $HygroMin): self
    {
        $this->HygroMin = $HygroMin;

        return $this;
    }

    public function getHygroMax(): ?string
    {
        return $this->HygroMax;
    }

    public function setHygroMax(string $HygroMax): self
    {
        $this->HygroMax = $HygroMax;

        return $this;
    }

    public function getOfficineID(): ?Officine
    {
        return $this->OfficineID;
    }

    public function setOfficineID(?Officine $OfficineID): self
    {
        $this->OfficineID = $OfficineID;

        return $this;
    }

    /**
     * @return Collection|DataTemp[]
     */
    public function getDataTemps(): Collection
    {
        return $this->dataTemps;
    }

    public function addDataTemp(DataTemp $dataTemp): self
    {
        if (!$this->dataTemps->contains($dataTemp)) {
            $this->dataTemps[] = $dataTemp;
            $dataTemp->setChambreFroideID($this);
        }

        return $this;
    }

    public function removeDataTemp(DataTemp $dataTemp): self
    {
        if ($this->dataTemps->removeElement($dataTemp)) {
            // set the owning side to null (unless already changed)
            if ($dataTemp->getChambreFroideID() === $this) {
                $dataTemp->setChambreFroideID(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataHygro[]
     */
    public function getDataHygros(): Collection
    {
        return $this->dataHygros;
    }

    public function addDataHygro(DataHygro $dataHygro): self
    {
        if (!$this->dataHygros->contains($dataHygro)) {
            $this->dataHygros[] = $dataHygro;
            $dataHygro->setChambreFroideID($this);
        }

        return $this;
    }

    public function removeDataHygro(DataHygro $dataHygro): self
    {
        if ($this->dataHygros->removeElement($dataHygro)) {
            // set the owning side to null (unless already changed)
            if ($dataHygro->getChambreFroideID() === $this) {
                $dataHygro->setChambreFroideID(null);
            }
        }

        return $this;
    }
}
