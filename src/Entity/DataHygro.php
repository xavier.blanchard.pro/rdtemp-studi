<?php

namespace App\Entity;

use App\Repository\DataHygroRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DataHygroRepository::class)
 */
class DataHygro
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateHeure;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $Valeur;

    /**
     * @ORM\Column(type="binary")
     */
    private $Validation;

    /**
     * @ORM\ManyToOne(targetEntity=ChambreFroide::class, inversedBy="dataHygros")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ChambreFroideID;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateHeure(): ?\DateTimeInterface
    {
        return $this->DateHeure;
    }

    public function setDateHeure(\DateTimeInterface $DateHeure): self
    {
        $this->DateHeure = $DateHeure;

        return $this;
    }

    public function getValeur(): ?string
    {
        return $this->Valeur;
    }

    public function setValeur(string $Valeur): self
    {
        $this->Valeur = $Valeur;

        return $this;
    }

    public function getValidation()
    {
        return $this->Validation;
    }

    public function setValidation($Validation): self
    {
        $this->Validation = $Validation;

        return $this;
    }

    public function getChambreFroideID(): ?ChambreFroide
    {
        return $this->ChambreFroideID;
    }

    public function setChambreFroideID(?ChambreFroide $ChambreFroideID): self
    {
        $this->ChambreFroideID = $ChambreFroideID;

        return $this;
    }
}
