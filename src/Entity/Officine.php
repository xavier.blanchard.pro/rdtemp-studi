<?php

namespace App\Entity;

use App\Repository\OfficineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfficineRepository::class)
 */
class Officine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $RaisonSociale;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Adresse;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $CodePostal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Ville;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $Telephone;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $UserID;

    /**
     * @ORM\OneToMany(targetEntity=ChambreFroide::class, mappedBy="OfficineID")
     */
    private $chambreFroide;

    public function __construct()
    {
        $this->chambreFroide = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRaisonSociale(): ?string
    {
        return $this->RaisonSociale;
    }

    public function setRaisonSociale(string $RaisonSociale): self
    {
        $this->RaisonSociale = $RaisonSociale;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->Adresse;
    }

    public function setAdresse(string $Adresse): self
    {
        $this->Adresse = $Adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->CodePostal;
    }

    public function setCodePostal(string $CodePostal): self
    {
        $this->CodePostal = $CodePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->Ville;
    }

    public function setVille(string $Ville): self
    {
        $this->Ville = $Ville;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->Telephone;
    }

    public function setTelephone(string $Telephone): self
    {
        $this->Telephone = $Telephone;

        return $this;
    }

    public function getUserID(): ?User
    {
        return $this->UserID;
    }

    public function setUserID(User $UserID): self
    {
        $this->UserID = $UserID;

        return $this;
    }

    /**
     * @return Collection|ChambreFroide[]
     */
    public function getChambreFroide(): Collection
    {
        return $this->chambreFroide;
    }

    public function addChambreFroide(ChambreFroide $chambreFroide): self
    {
        if (!$this->chambreFroide->contains($chambreFroide)) {
            $this->chambreFroide[] = $chambreFroide;
            $chambreFroide->setOfficineID($this);
        }

        return $this;
    }

    public function removeChambreFroide(ChambreFroide $chambreFroide): self
    {
        if ($this->chambreFroide->removeElement($chambreFroide)) {
            // set the owning side to null (unless already changed)
            if ($chambreFroide->getOfficineID() === $this) {
                $chambreFroide->setOfficineID(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
	    return $this->RaisonSociale;
    }
}
